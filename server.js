"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
mongoose.connect("mongodb://localhost:27017/movie_db");
const hash = require('./hash')
const db = mongoose.connection;
const cost =10;
db.on("error", err => {
  console.error(err);
});
db.once("open", () => {
  const app = express();
  const port = 8000;
  const Movie = require("./models/movie");
  const User = require("./models/user");
  const setUserMw = async (req, res, next) => {
    try {
      const user = await User.findOne({ email: "nut@gmail.com" }).exec();
      req.user = user;
      next();
    } catch (error) {
      console.log(error);
      res.status(500).send("no user");
    }
  };

  app.use(cors());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  app.get("/movies", async (req, res) => {
    const movieList = await Movie.find().exec();
    const results = {
      results: movieList
    };
    res.json(results);
  });

  app.post("/signup", async (req, res) => {
    let { email, password } = req.body;
    try {
      password=await hash.genHash(password,cost)
      await User.create({ email, password });
      res.sendStatus(200);
    } catch (err) {
      console.log(err);
    }
  });

  app.get("/favorite", (req, res) => {
    const results = {
      results: favorites
    };
    res.json(results);
  });

  app.post("/favorite", (req, res) => {
    console.log(req.body);
    const fav = {
      id: ++counter,
      title: req.body.title
    };
    favorites.push(fav);
    res.json(fav);
  });
  app.delete("/favorites/:id", (req, res) => {
    const id = parseInt(req.params.id, 10);
    const index = favorites.find(fav => fav.id === id);
    if (index === -1) {
      res.sendStatus(404);
    } else {
      favorites.splice(index, 1);
      res.sendStatus(200);
    }
  });

  app.listen(port, () => {});
});
