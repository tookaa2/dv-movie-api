const hash = require("bcrypt-nodejs");

exports.genHash = (password, cost) => {
  return new Promise((resolve, reject) => {
    hash.genSalt(cost, (err, salt) => {
      if (err) {
        reject(err);
      }
      hash.hash(password, salt,null, (err, result) => {
        if (err) {
          reject(err);
        }
        resolve(result);
      });
    });
  });
};

